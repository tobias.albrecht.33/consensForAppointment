import csv
import os
import platform
import sys
from argparse import ArgumentParser, RawDescriptionHelpFormatter
module_name = "Consens: Find suiting appointments for large groups"
__version__ = "0.0.1"

def main():

    version_string = f"%(prog)s {__version__}\n" +  \
                     f"Python:  {platform.python_version()}"

    parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter,
                            description=f"{module_name} (Version {__version__})"
                            )
    parser.add_argument("--version",
                        action="version",  version=version_string,
                        help="Display version information and dependencies."
                        )
    parser.add_argument("--verbose", "-v", "-d", "--debug",
                        action="store_true",  dest="verbose", default=False,
                        help="Display extra debugging information and metrics."
                        )
    parser.add_argument("--no-color",
                        action="store_true", dest="no_color", default=False,
                        help="Don't color terminal output"
                        )
    parser.add_argument("--csv-in", "-f", metavar="CSV_FILE",
                        dest="csv_file", default=None,
                        help="Load data from a CSV file."
                        )
    parser.add_argument("--out", "-o", dest="output",
                        help="Pipe the output to a file instead Stout."
                        )
    args = parser.parse_args()

if __name__ == "__main__":
    main()
